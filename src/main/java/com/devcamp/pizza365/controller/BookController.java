package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Book;
import com.devcamp.pizza365.model.BookId;
import com.devcamp.pizza365.repository.IBookRepository;


@CrossOrigin
@RestController
public class BookController {
    @Autowired
    IBookRepository pBookRepository;

   @GetMapping("/book")
    public ResponseEntity<List<Book>> getAllBook() {
        try {
            ArrayList<Book> lstBooks = new ArrayList<>();
            pBookRepository.findAll().forEach(lstBooks::add);

            if (lstBooks.size() == 0) {
                return new ResponseEntity<List<Book>>(lstBooks, HttpStatus.NOT_FOUND);
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(lstBooks);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            // TODO: handle exception
        }
    }    

   @PostMapping("/book")
    public ResponseEntity<Book> createBook(@RequestBody Book book) {
        try {
            Book newBook = pBookRepository.save(book);
            return new ResponseEntity<>(newBook, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            
        }
    }  
    
   @PostMapping("/book2")
    public ResponseEntity<Book> createNewBook() {
        try {
            BookId bookId = new BookId("nodejs", "Vietnamese");
            Book book = new Book(bookId, 150000);
            Book newBook = pBookRepository.save(book);
            return new ResponseEntity<>(newBook, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);            
        }
    }     
}
