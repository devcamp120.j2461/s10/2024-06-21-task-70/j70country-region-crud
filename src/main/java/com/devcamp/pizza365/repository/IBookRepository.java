package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.Book;
import com.devcamp.pizza365.model.BookId;

public interface IBookRepository extends JpaRepository<Book, BookId> {
    
}
